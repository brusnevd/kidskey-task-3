let line, activeIndex = 0,
    currentPos, pointsContainer = [];

const POINTS = [{
        x: 300,
        y: 400,
    },
    {
        x: 300,
        y: 500
    },
    {
        x: 400,
        y: 500
    },
    {
        x: 400,
        y: 400
    },
    {
        x: 400,
        y: 500
    },
]

let app = new PIXI.Application({
    width: 1000,
    height: 600
});

app.renderer.backgroundColor = 0xE4E4E4;

document.body.appendChild(app.view);

function displayPoints(points) {
    points.forEach((point, index) => {
        let container = new PIXI.Container

        container.x = point.x;
        container.y = point.y;

        let circleArea = new PIXI.Graphics();
        circleArea.beginFill(app.renderer.backgroundColor);
        circleArea.drawCircle(0, 0, 20);
        circleArea.endFill();

        circleArea.position.set(point.x, point.y);

        if (index == activeIndex) {
            circleArea.interactive = true;
            circleArea.buttonMode = true;
            circleArea
                .on("pointerdown", onDragStart)
                .on("pointerup", onDragEnd)
                .on("pointerupoutside", onDragEnd)
                .on("pointermove", onDragMove);
        }

        let message = new PIXI.Text((index + 1).toString(), { fontSize: 15 });
        message.position.set(-20, -20);
        let circle = new PIXI.Graphics();
        circle.beginFill(0x000000);
        circle.drawCircle(0, 0, 3);
        circle.endFill();

        container.addChild(circle);
        container.addChild(message);

        app.stage.addChild(circleArea);
        app.stage.addChild(container);

        pointsContainer.push(circleArea);
    });
}



function onDragStart(event) {
    oldPosX = this.x;
    oldPosY = this.y;
    this.data = event.data;
    this.event = event;
    this.dragging = true;
    line = new PIXI.Graphics();
}

function onDragEnd() {
    this.alpha = 1;
    this.dragging = false;
    this.data = null;

    if (pointsContainer[activeIndex + 1].containsPoint(currentPos)) {
        line.destroy()
        let correctLine = new PIXI.Graphics();
        correctLine.lineStyle(4, 0x000000, 1);
        correctLine.moveTo(this.x, this.y);
        correctLine.lineTo(pointsContainer[activeIndex + 1].x, pointsContainer[activeIndex + 1].y);
        app.stage.addChild(correctLine);
        if (activeIndex == Object.keys(pointsContainer).length - 3) {
            pointsContainer[activeIndex].interactive = false;
            pointsContainer[activeIndex].buttonMode = false;
        } else {
            pointsContainer[activeIndex].interactive = false;
            pointsContainer[activeIndex].buttonMode = false;
            pointsContainer[++activeIndex].interactive = true;
            pointsContainer[activeIndex].buttonMode = true;
            pointsContainer[activeIndex]
                .on("pointerdown", onDragStart)
                .on("pointerup", onDragEnd)
                .on("pointerupoutside", onDragEnd)
                .on("pointermove", onDragMove);
        }
    } else {
        line.destroy()
    }
}

function onDragMove() {
    if (this.dragging) {
        line.destroy();
        const newPos = this.data.getLocalPosition(this.parent);
        line = new PIXI.Graphics();
        line.lineStyle(4, 0x000000, 1);
        line.moveTo(newPos.x, newPos.y);
        line.lineTo(oldPosX, oldPosY);
        currentPos = {
            x: newPos.x,
            y: newPos.y
        }
        app.stage.addChild(line);
    }
}

displayPoints(POINTS);